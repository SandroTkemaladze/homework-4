package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstVariable = 0.0
    private var secondVariable = 0.0
    private var operation = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
           init()
    }


    private fun init(){
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

        dotButton.setOnClickListener {
            resultTextView.text = resultTextView.text.toString() + "."

        }
    }

    fun divide(view:View){
        val value = resultTextView.text.toString()
        operation = ":"
        if(value.isNotEmpty())
        firstVariable = value.toDouble()
        resultTextView.text = ""


    }

    fun equals(view: View) {
        val value = resultTextView.text.toString()
        secondVariable = value.toDouble()

        var result:Double = 0.0
        if (operation == ":") {
            result = firstVariable / secondVariable
        } else if (operation == "+") {
            result = firstVariable + secondVariable
        } else if(operation == "X") {
            result = firstVariable * secondVariable
        } else if(operation == "-") {
            result = firstVariable - secondVariable
        }

    }



    fun delete(view: View){
        val value:String = resultTextView.text.toString()
        if (value.isNotEmpty())
        resultTextView.text = value.substring(0,value.length - 1)
    }

    override fun onClick(v: View?) {
    val button:Button = v as Button
    resultTextView.text = resultTextView.text.toString() + button.text.toString()
    }

}